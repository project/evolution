<?php 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">   
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
  </head>
  <body class="<?php print $body_classes ?>">
    <div id="container">

      <div id="header">
        <?php // print '<span id="logo"><a href="' . check_url($base_path) . '" title="' . $site_name . '">' . $site_name . '</a></span>'; ?>
        <?php print $breadcrumb; ?>
        <?php print $header; ?>
      </div> <!-- End #header -->

      <div id="secondary" class="collapsed">
        <div id="secondary-wrapper">
          <?php print $admin_menu ?>
          <?php print $secondary ?>
        </div>
      </div> <!-- End #secondary -->      
 
      <div id="wrapper">

        <div id="primary-top"></div>
        <div id="primary">
          <?php if ($tabs): print $tabs; endif; ?>
          <?php if ($title): print '<h1>'. $title .'</h1>'; endif; ?>
          <?php if ($help): print $help; endif; ?>
          <?php if ($messages): print $messages; endif; ?>
          <?php print $content ?>
          <span class="clear-block"></span>
          <?php print $feed_icons ?>
        </div>  <!-- End #primary -->
        <div id="primary-bottom"></div>
      </div> <!-- End #wrapper -->
      
    </div> <!-- End #container -->
    
    <?php print $scripts ?>
    <?php print $closure ?>
  </body>
</html>
