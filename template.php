<?php

/**
 * Preprocess page templates.
 */
function evolution_preprocess_page(&$vars) {
  require_once(drupal_get_path('module', 'system') . '/system.admin.inc');
  $vars['admin_menu'] = system_main_admin_page();
}
