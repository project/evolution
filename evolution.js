
/**
 * Toggle the panel, menu descriptions, help text, messages, etc.
 */
Drupal.behaviors.evoToggle = function(context) {
    $('#secondary').before('<a class="expand" title="Expand the drop down menu" href="#">&darr; Expand Menu &darr;</a>');
	$('#secondary').before('<a class="contract" title="Close the drop down menu" href="#">X</a>')
	$('#secondary').before('<a class="descriptions" title="Toggle descriptions on the menu items" href="#">Toggle Descriptions</a>')
	$('.messages').prepend('<a class="hide-messages" title="Hide this message" href="#">X</a>')
	if ($('.help').length) {
    	$('#header').prepend('<a class="show-help" title="Show the help text for this page" href="#">?</a>')
	}
	
	$('.expand, .contract').click(function(){ 
		if ($('#secondary').hasClass('collapsed')) {
			$('#secondary').slideDown().removeClass('collapsed');
			$('a.expand').hide();
			$('a.contract, a.descriptions').show();
		} else {
			$('#secondary').slideUp().addClass('collapsed');
			$('a.contract, a.descriptions').hide();
			$('a.expand').show();
		}
		return false;
	});
	$('.show-help').click(function(){
	    $('div.help').toggle();
	})
	$('.descriptions').click(function(){
	    $('#secondary dd').toggle();
	})
	$('.hide-messages').click(function(){
	    $(this).parent().hide();
	});
};

/**
 * Display form item descriptions when in focus.
 */
Drupal.behaviors.expandFormItemDescriptions = function(context) {
	var selectors = []
	$.each(['input', 'textarea', 'select'], function(i, element){ 
		selectors.push('.form-item ' + element + ':not(.expandFormItemDescriptions-processed)');
	});
	$(selectors.join(',')).addClass('expandFormItemDescriptions-processed').focus(function(){
	    if (!$(this).parents('.form-item').children('.description').hasClass('visible')) {
    		$('.form-item .description.visible').slideUp(400).removeClass('visible');
		    $(this).parents('.form-item').children('.description').slideDown(400).addClass('visible');
	    }
	});
};

/**
 * Creates tabs from top-level fieldsets for a more intuitive UI. 
 */
Drupal.behaviors.createTabsFromFieldsets = function(context) {
	// Collect top-level fieldsets and hide them initially
	var fieldsets = $('form > div > fieldset, .theme-settings-left fieldset, .theme-settings-right fieldset')
	
	// Only one or less fieldsets are available
	if (fieldsets.length <= 1)
		return;
		
	fieldsets.addClass('evolution-processed');

	// Build tab markup
	$(fieldsets).hide()
	var total = fieldsets.length;
	var tabs = '<div class="tabs-wrapper"><ul class="tabs fieldset-tabs">';
	$(fieldsets).each(function(i, fieldset) {
		classes = []
		if (i == 0){ classes.push('first'); classes.push('active') }
		if (i == (total - 1)) { classes.push('last') }
		var title = $('legend', fieldset).html();
		title = title.replace(/\<(?:[^>]+)\>/, '');
		tabs += '<li class="' + classes.join(' ') + '"><a href="#fieldset-tab-' + i + '">' + title + '</a></li>'
		$(fieldset).attr('id', 'fieldset-tab-' + i).addClass('fieldset-tab');
	});
	tabs += '</ul><div class="clear"></div></div><!-- End .tabs-wrapper -->';
	
	// Add generated tab markup
	tabs = $(tabs);
	$('#primary').prepend(tabs);
	$($(fieldsets).get(0)).fadeIn(400).removeClass('collapsed');
	$('form > div > fieldset > legend', $(fieldsets).get(0)).remove();
	
	// Toggle tabs
	$('a', tabs).click(function(){
	    if ($(this).parent().hasClass('active')){
	        return false;
	    }
		$('li.active', tabs).removeClass('active');
		$(this).parents('li').addClass('active');
		$(fieldsets).hide(); 
		var fieldsetID = '#' + $(this).attr('href').replace('#', '');
		$(fieldsetID).removeClass('collapsed').fadeIn(400);
		$(fieldsetID + ' > legend').remove();
		return false;
	});
};
